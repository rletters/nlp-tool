package net.rletters.nlp_tool;

import java.io.InputStream;
import java.io.StringWriter;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import junit.framework.TestCase;
import net.rletters.nlp_tool.Lemmatizer;

public class LemmatizerTest extends TestCase {
  public LemmatizerTest(String name) {
    super(name);
  }

  public void testParser() throws Exception {
    InputStream emma = getClass().getClassLoader().getResourceAsStream("jane-austen-emma-ch2.txt");
    StringWriter writer = new StringWriter();
    IOUtils.copy(emma, writer, StandardCharsets.UTF_8);
    String input = writer.toString();

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(baos);
    System.setOut(ps);

    Lemmatizer p = new Lemmatizer();
    p.printLemmatizedWords(input);

    String output = baos.toString();

    assertTrue(output.contains("- \"hear\""));
    assertFalse(output.contains("- \"heard\""));
    assertTrue(output.contains("- \"write\""));
    assertFalse(output.contains("- \"written\""));
  }
}
