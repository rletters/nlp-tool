package net.rletters.nlp_tool;

import junit.framework.TestCase;
import net.rletters.nlp_tool.YAMLCleaner;

public class YAMLCleanerTest extends TestCase {
  public YAMLCleanerTest(String name) {
    super(name);
  }

  public void testClean() throws Exception {
    assertEquals("\"asdf\"", YAMLCleaner.clean("asdf"));
    assertEquals("\"a\\\\s\\\"df\"", YAMLCleaner.clean("a\\s\"df"));
  }
}
