package net.rletters.nlp_tool;

import java.io.InputStream;
import java.io.StringWriter;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import junit.framework.TestCase;
import net.rletters.nlp_tool.NERParser;

public class NERParserTest extends TestCase {
  public NERParserTest(String name) {
    super(name);
  }

  public void testParser() throws Exception {
    InputStream emma = getClass().getClassLoader().getResourceAsStream("jane-austen-emma-ch2.txt");
    StringWriter writer = new StringWriter();
    IOUtils.copy(emma, writer, StandardCharsets.UTF_8);
    String input = writer.toString();

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(baos);
    System.setOut(ps);

    NERParser p = new NERParser();
    p.printNamedEntities(input);

    String output = baos.toString();

    assertTrue(output.startsWith("---\n"));
    assertTrue(output.contains("PERSON:"));
    assertTrue(output.contains("LOCATION:"));
    assertTrue(output.contains("ORGANIZATION:"));
    assertTrue(output.contains("    - \"London\""));
  }
}
