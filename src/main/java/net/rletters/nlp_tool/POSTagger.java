package net.rletters.nlp_tool;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.util.Triple;

public class POSTagger {
  public void printTaggedString(String text) {
    MaxentTagger tagger = new MaxentTagger(DefaultPaths.DEFAULT_POS_MODEL);
    String[] result = tagger.tagString(text).split("\\s+");

    System.out.println("---");
    for (String hit : result) {
      System.out.println("- " + YAMLCleaner.clean(hit));
    }
  }
}
