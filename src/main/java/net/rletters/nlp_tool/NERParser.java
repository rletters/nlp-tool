package net.rletters.nlp_tool;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.util.Triple;

public class NERParser {
  public void printNamedEntities(String text) {
    CRFClassifier classifier = CRFClassifier.getClassifierNoExceptions(DefaultPaths.DEFAULT_NER_THREECLASS_MODEL);
    List<Triple<String, Integer, Integer>> triples = classifier.classifyToCharacterOffsets(text);
    HashMap<String, HashSet<String>> result = new HashMap<>();

    for(Triple<String, Integer, Integer> entity : triples) {
      String target = text.substring(entity.second, entity.third).replaceAll("[\r\n\t]", " ");
      String key = entity.first;

      HashSet<String> newSet;
      if (result.containsKey(key)) {
        newSet = result.get(key);
      } else {
        newSet = new HashSet<String>();
      }
      newSet.add(target);
      result.put(key, newSet);
    }

    System.out.println("---");
    for (Map.Entry<String, HashSet<String>> entry : result.entrySet()) {
      System.out.println(entry.getKey() + ":");

      for (String match : entry.getValue()) {
        System.out.println("    - " + YAMLCleaner.clean(match));
      }
    }
  }
}
