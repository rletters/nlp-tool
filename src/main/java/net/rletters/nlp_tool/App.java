package net.rletters.nlp_tool;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;

public class App {
  public static void main(String[] args) {
    Option help = new Option("h", "help", false, "display this help message");
    Option ner = new Option("n", "ner", false, "extract named entities from stdin");
    Option pos = new Option("p", "pos", false, "tag parts of speech from stdin");
    Option lemma = new Option("l", "lemma", false, "print lemmatized words from stdin");

    Options options = new Options();
    options.addOption(help);
    options.addOption(ner);
    options.addOption(pos);
    options.addOption(lemma);

    CommandLineParser parser = new GnuParser();
    CommandLine line = null;
    try {
      line = parser.parse(options, args);
    }
    catch(ParseException e) {
      System.err.println("Error: Cannot parse command line: " + e.getMessage());
      System.exit(1);
    }

    if ((!line.hasOption("n") && !line.hasOption("p") &&
         !line.hasOption("l")) ||
        line.hasOption("h")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("nlp-tool [OPTION]... FILE", options);
      System.exit(1);
    }

    try {
      StringWriter writer = new StringWriter();
      IOUtils.copy(System.in, writer, StandardCharsets.UTF_8);
      String input = writer.toString();

      if (line.hasOption("n")) {
        NERParser p = new NERParser();
        p.printNamedEntities(input);
      } else if (line.hasOption("p")) {
        POSTagger p = new POSTagger();
        p.printTaggedString(input);
      } else if (line.hasOption("l")) {
        Lemmatizer p = new Lemmatizer();
        p.printLemmatizedWords(input);
      }
    } catch(IOException e) {
      System.err.println("Error: IO exception: " + e.getMessage());
      System.exit(1);
    }
  }
}
