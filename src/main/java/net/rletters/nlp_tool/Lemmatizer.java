package net.rletters.nlp_tool;

import java.util.Properties;
import java.util.List;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;

public class Lemmatizer {
  public void printLemmatizedWords(String text) {
    Properties props = new Properties();
    props.put("annotators", "tokenize, ssplit, pos, lemma");
    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

    Annotation document = new Annotation(text);
    pipeline.annotate(document);

    System.out.println("---");

    List<CoreLabel> tokens = document.get(CoreAnnotations.TokensAnnotation.class);
    for (CoreLabel token : tokens) {
      String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
      System.out.println("- " + YAMLCleaner.clean(lemma));
    }
  }
}
